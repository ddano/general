#!/bin/bash

#ansible kaltura-all -m shell -a 'mkdir /apps/tasteos' --inventory-file=/etc/ansible/environment/prod/hosts
#ansible kaltura-all -m shell -a 'chown a77504404:SK002761 /apps/tasteos' --inventory-file=/etc/ansible/environment/prod/hosts

echo "## Want to delete old scripts/result on target machines ? ## --y/n-- "
read cleanup
if [ "$cleanup" = "y" ]; then
ansible kaltura-all -m shell -a 'rm /apps/tasteos/*' --inventory-file=/etc/ansible/environment/prod/hosts
    if [ "$cleanup" = "n" ]; then
        echo "Scripts/results not deleted"
    fi
fi


echo "## Want to copy scripts ? ## -- y/n -- "
read theny

if [ "$theny" = "y" ]; then
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23469482.sh a77504404@qdexy7.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23469486.sh a77504404@qdexzz.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23469694.sh a77504404@qdewdv.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23469698.sh a77504404@qdewdy.de.t-internal.com:/apps/tasteos/
cp /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470093.sh /apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23469755.sh a77504404@qdewe0.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470069.sh a77504404@qdewe2.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470073.sh a77504404@qdewe4.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470077.sh a77504404@qdewdc.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470081.sh a77504404@qdewdf.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470085.sh a77504404@qdewdm.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470089.sh a77504404@qdewdp.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470112.sh a77504404@qdewd4.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470116.sh a77504404@qdewd7.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470158.sh a77504404@qdewd9.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470197.sh a77504404@qde0wt.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470201.sh a77504404@qdewca.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470205.sh a77504404@qdewcm.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23470209.sh a77504404@qdewcp.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23485096.sh a77504404@qdewg7.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23485116.sh a77504404@qdewg9.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23695242.sh a77504404@edefcy.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23695248.sh a77504404@edefcz.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23695253.sh a77504404@edefd0.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx23695257.sh a77504404@edefd1.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx24209409.sh a77504404@edeta3.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx24209413.sh a77504404@edeta4.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx24209417.sh a77504404@edeta5.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-dcplnx24209421.sh a77504404@edeta6.de.t-internal.com:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-qdefr4.sh a77504404@10.91.92.234:/apps/tasteos/
scp -v /home/admin/a77504404/tasteos_scripts/collectorScript-ederl3.sh a77504404@10.91.92.229:/apps/tasteos/

    if [ "$theny" = "n" ]; then
        echo "Scripts not copied"
    fi
fi

echo "## Want to chmod scripts ? ## -- y/n -- "
read chmody
if [ "$chmody" = "y" ]; then
ansible kaltura-all -m shell -a 'chmod +x /apps/tasteos/collectorScript-*.sh' --inventory-file=/etc/ansible/environment/prod/hosts
    if [ "$chmody" = "n" ]; then
        echo "Chmod not executed"
    fi
fi

echo "## Want to execute scripts ? ## -- y/n -- "
read execute

if [ "$execute" = "y" ]; then
ansible kaltura-all -m shell -a '/apps/tasteos/collectorScript-*.sh > /apps/tasteos/result_$(hostname).txt' --inventory-file=/etc/ansible/environment/prod/hosts
    if [ "$execute" = "n" ]; then
        echo "Scripts not executed"
    fi
fi


echo "## Want to collect result ? ## -- y/n -- "
read collect

if [ "$collect" = "y" ]; then
scp -v a77504404@qdexy7.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdexzz.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewdv.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewdy.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewe0.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewe2.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewe4.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewdc.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewdf.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewdm.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewdp.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewd4.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewd7.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewd9.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qde0wt.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewca.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewcm.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewcp.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewg7.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@qdewg9.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@edefcy.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@edefcz.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@edefd0.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@edefd1.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@edeta3.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@edeta4.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@edeta5.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@edeta6.de.t-internal.com:/apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/ 
scp -v a77504404@10.91.92.234:/apps/tasteos/result_*.txt	/home/admin/a77504404/tasteos_results/ 
scp -v a77504404@10.91.92.229:/apps/tasteos/result_*.txt	/home/admin/a77504404/tasteos_results/ 
cp /apps/tasteos/result_*.txt /home/admin/a77504404/tasteos_results/
    if [ "$collect" = "n" ]; then
         echo "Scripts not collected"
     fi
fi
