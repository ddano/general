#! /bin/bash

GR='\033[0;32m'
RD='\033[0;31m'
NC='\033[0m'

#os=$(lsb_release -d -r)
os=$(lsb_release -d| cut -f 2)
hostname=$(hostname)
ip=$(hostname -I | cut -d ' ' -f 1,2)
dnshost=$(nslookup $(hostname -I | cut -d ' ' -f 1) | cut -d ' ' -f 3 | head -1)
memory=$(free -h | grep -v "Swap")
swap=$(free -h | grep -v "Mem")
load=$( top -n 1 -b | grep "load average:" | awk '{print $12 $13 $14 }')
uptime=$(uptime | awk '{print $3,$4}' | cut -f1 -d,)
fs=$( df -h| grep 'Filesystem\|/apps*')
apache=$(rpm -qa httpd)
mariadb=$(rpm -qa mariadb)
haproxy=$(rpm -qa haproxy)
activemq=$(/apps/activemq/bin/activemq status 2> /dev/null | grep -o "ActiveMQ is running" )
wildfly=$(sudo /apps/wildfly/bin/jboss-cli.sh --controller=10.105.178.207:9990 --connect -c command=':read-attribute(name=server-state)' 2> /dev/null | grep '"running"')

case "$1" in
        os)
                echo -e "${GR}OS ${NC}: $test2 $os \n"
                ;;
        hostname)
                echo -e "${GR}HOSTNAME ${NC}: $hostname \n"
                ;;
        ip)
                echo -e "${GR}INTERNAL IP ${NC}: $ip \n"
                ;;
        dns)
                echo -e "${GR}DNS HOSTNAME ${NC}: $dnshost \n"
                ;;
        memory)
                echo -e "${GR}MEMORY ${NC}:\n $memory \n"
                ;;
        swap)
                echo -e "${GR}SWAP ${NC}:\n $swap \n"
                ;;
        load)
                echo -e "${GR}LOAD ${NC}: $load \n"
                ;;
        uptime)
                echo -e "${GR}UPTIME ${NC}: $uptime \n"
                ;;
        fs)
                echo -e "${GR}APPS ${NC}:\n $fs \n"
                ;;
        mysql | mariadb)
                if [[ -z $mariadb ]];then
                        echo "Mysql not installed"
                else
                        echo -n -e "Mysql ${GR} installed ${NC}&& " && systemctl is-active --quiet mariadb && echo -e "is ${GR}running.${NC}" || echo -e "is ${RD}NOT running.${NC}"
                fi
                ;;
        httpd | apache)
                if [[ -z $apache ]];then
                        echo "Apache not installed"
                else
                        echo -n -e "Apache ${GR} installed ${NC}&& " && systemctl is-active --quiet httpd && echo -e "is ${GR}running.${NC}" || echo -e "is ${RD}NOT running.${NC}"
                fi
                ;;
        memcached)
                if [[ ! -e /usr/bin/memcached ]];then
		        echo "Memcahced not installed."
	        else
		        echo -n -e "Memcached\t${GR}installed ${NC}&& " && systemctl is-active --quiet memcached && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
	        fi
                ;;
        moxi)
                if [[ ! -e /opt/moxi/bin/moxi ]];then
		        echo "Moxi not installed."
	        else
		        echo -n -e "Moxi\t\t${GR}installed ${NC}&& " && systemctl is-active --quiet moxi-server && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
	        fi
                ;;
        decoder)
                if [[ ! -e /apps/asrdec/emlDecoder/emltd ]];then
                        echo "EML Decoder not installed. Available only on DECODER servers."
                else
                        echo -n -e "EML Decoder\t\t${GR}installed ${NC}&& " && systemctl is-active --quiet emltd && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
                fi
                ;;
        haproxy)
                if [[ -z $haproxy ]];then
                        echo "Haproxy not installed.  Available only on PROXY servers."
                else
                        echo -n -e "Haproxy\t\t${GR}installed ${NC}&&" && systemctl is-active --quiet haproxy && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
                fi
                ;;
        asrbridge)
                if [[ ! -e /apps/asrbridge/asr-bridge-0.3.2.jar ]];then
                        echo "ASR-Bridge not installed. Available only on SWISSTXT servers."
                else
                        echo -n -e "ASR-Bridge\t\t${GR}installed ${NC}&&" && systemctl is-active --quiet asr-bridge && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
                fi
                ;;
        asrgateway)
                if [[ ! -e /apps/asrgateway/asr-gateway-0.3.0-0.jar ]];then
                        echo "ASR-Gateway not installed. Available only on SWISSTXT servers."
                else
                        echo -n -e "ASR-Gateway\t\t${GR}installed ${NC}&&" && systemctl is-active --quiet asr-gateway && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
                fi
                ;;
        captions | captionsconn | captionsconnector)
                if [[ ! -e /apps/captionsconn/captions-connector-0.3.0.jar ]];then
                        echo "Captions connector not installed. Available only on SWISSTXT servers."
                else
                        echo -n -e "Captions connector\t\t${GR}installed ${NC}&&" && systemctl is-active --quiet captions-connector && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
                fi
                ;;
        subtitleed | subeditor)
                if [[ ! -e /apps/subtitleed/subtitle-editor-0.3.0.jar ]];then
                        echo "SUB-Editor connector not installed. Available only on SWISSTXT servers."
                else
                        echo -n -e "SUB-Editor connector\t\t${GR}installed ${NC}&&" && systemctl is-active --quiet subtitle-editor.service && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
                fi
                ;;
        activemq)
                if [[ ! -e /apps/activemq/bin/activemq ]];then
                        echo "ActiveMQ not installed. Available only on ASRMNG servers."
                elif [[ -n $activemq ]];then
                        echo -e "ActiveMQ \t\t${GR}installed ${NC}&& is ${GR}running${NC}."
                else
                        echo -e "ActiveMQ \t\t${GR}installed ${NC}&& is ${RD}NOT running${NC}."
                fi
                ;;
        wildfly)
                if [[ ! -e /apps/wildfly/bin/standalone.sh ]];then
                        echo "Wildfly not installed. Available only on ASRMNG servers."
                elif [[ -n $wildfly ]];then
                        echo -e "Wildfly \t\t${GR}installed ${NC}&& is ${GR}running${NC}."
                else
                        echo -e "Wildfly \t\t${GR}installed ${NC}&& is ${RD}NOT running${NC}."
                fi
                ;;
        *)
                echo "Unknown argument : $1"
                echo "Optional arguments - os / hostname / ip / dns /memory / swap / load / uptime / fs / apache*httpd / mariadb*mysql / haproxy / asrbridge / asrgateway / captions*captionsconn*captionsconnector / subtitleed*subeditor / activemq / wildfly"
                ;;
esac


if [ -z $1 ];then
        clear
        echo -e "${GR}OS ${NC}: $test2 $os \n"
        echo -e "${GR}HOSTNAME ${NC}: $hostname \n"
        echo -e "${GR}INTERNAL IP ${NC}: $ip \n"
        echo -e "${GR}DNS HOSTNAME ${NC}: $dnshost \n"
        echo -e "${GR}MEMORY ${NC}:\n $memory \n"
        echo -e "${GR}SWAP ${NC}:\n $swap \n"
        echo -e "${GR}LOAD ${NC}: $load \n"
        echo -e "${GR}UPTIME ${NC}: $uptime \n"
        echo -e "${GR}APPS ${NC}:\n $fs \n"
#SERVICE CHECK
#MARIADB
        echo -n -e "${GR}SERVICE CHECK ${NC}:\n"
        if [[ -z $apache ]];then
              echo "Mysql not installed."
        else
              echo -n -e "Mysql\t\t${GR}installed ${NC}&& " && systemctl is-active --quiet mariadb && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
        fi
#APACHE CHECK
        if [[ -z $apache ]];then
              echo "Apache not installed."
        else
              echo -n -e "Apache\t\t${GR}installed ${NC}&& " && systemctl is-active --quiet httpd && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
        fi
#MEMCACHED CHECK
	if [[ ! -e /usr/bin/memcached ]];then
		echo "Memcahced not installed."
	else
		echo -n -e "Memcached\t${GR}installed ${NC}&& " && systemctl is-active --quiet memcached && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
	fi
#MOXI
	if [[ ! -e /opt/moxi/bin/moxi ]];then
		echo "Moxi not installed."
	else
		echo -n -e "Moxi\t\t${GR}installed ${NC}&& " && systemctl is-active --quiet moxi-server && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
	fi
#DECODER
        if [[ ! -e /apps/asrdec/emlDecoder/emltd ]];then
                echo "EML Decoder not installed. Available only on DECODER servers."
        else
                echo -n -e "EML Decoder\t\t${GR}installed ${NC}&& " && systemctl is-active --quiet emltd && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
        fi
#HAPROXY
        if [[ -z $haproxy ]];then
                echo "Haproxy not installed.  Available only on PROXY servers."
        else
                echo -n -e "Haproxy\t\t${GR}installed ${NC}&&" && systemctl is-active --quiet haproxy && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
        fi
#ASRBRIDGE
        if [[ ! -e /apps/asrbridge/asr-bridge-0.3.2.jar ]];then
                echo "ASR-Bridge not installed. Available only on SWISSTXT servers."
        else
                echo -n -e "ASR-Bridge\t\t${GR}installed ${NC}&&" && systemctl is-active --quiet asr-bridge && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
        fi
#ASRGATEWAY
        if [[ ! -e /apps/asrgateway/asr-gateway-0.3.0-0.jar ]];then
                echo "ASR-Gateway not installed. Available only on SWISSTXT servers."
        else
                echo -n -e "ASR-Gateway\t\t${GR}installed ${NC}&&" && systemctl is-active --quiet asr-gateway && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
        fi
#CAPTIONSCON
        if [[ ! -e /apps/captionsconn/captions-connector-0.3.0.jar ]];then
                echo "Captions connector not installed. Available only on SWISSTXT servers."
        else
                echo -n -e "Captions connector\t\t${GR}installed ${NC}&&" && systemctl is-active --quiet captions-connector && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
        fi
#SUBEDITOR
        if [[ ! -e /apps/subtitleed/subtitle-editor-0.3.0.jar ]];then
                echo "SUB-Editor connector not installed. Available only on SWISSTXT servers."
        else
                echo -n -e "SUB-Editor connector\t\t${GR}installed ${NC}&&" && systemctl is-active --quiet subtitle-editor.service && echo -e "is ${GR}running${NC}." || echo -e "is ${RD}NOT running${NC}."
        fi
#ACTIVEMQ
        if [[ ! -e /apps/activemq/bin/activemq ]];then
                echo "ActiveMQ not installed. Available only on ASRMNG servers."
        elif [[ -n $activemq ]];then
                echo -e "ActiveMQ \t\t${GR}installed ${NC}&& is ${GR}running${NC}."
        else
                echo -e "ActiveMQ \t\t${GR}installed ${NC}&& is ${RD}NOT running${NC}."
        fi
#WILDFLY
        if [[ ! -e /apps/wildfly/bin/standalone.sh ]];then
                echo "Wildfly not installed. Available only on ASRMNG servers."
        elif [[ -n $wildfly ]];then
                echo -e "Wildfly \t\t${GR}installed ${NC}&& is ${GR}running${NC}."
        else
                echo -e "Wildfly \t\t${GR}installed ${NC}&& is ${RD}NOT running${NC}."
        fi

fi
