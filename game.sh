#!/bin/bash
F_VDOBLE="\033#6"
screen_output() {
 printf "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
 printf "\t\t\t${F_VDOBLE}*-----------*\n"
 printf "\t\t\t${F_VDOBLE}| ${arr[1]} | ${arr[2]} | ${arr[3]} |\n"
 printf "\t\t\t${F_VDOBLE}*-----------*\n"
 printf "\t\t\t${F_VDOBLE}| ${arr[4]} | ${arr[5]} | ${arr[6]} |\n"
 printf "\t\t\t${F_VDOBLE}*-----------*\n"
 printf "\t\t\t${F_VDOBLE}| ${arr[7]} | ${arr[8]} | ${arr[0]} |\n"
 printf "\t\t\t${F_VDOBLE}*-----------*\n"
 printf "\n\n\n\n\n\n"
}

shuffle() {
max=9
i=1
random=0
arr[0]="X"
for i in 1; do
   random=$(shuf -i 1-8)
   arr+=($random)
done
}
arrp=(X 1 2 3 4 5 6 7 8)
move(){
read -s -r -n1 mv
if [[ ${arr[0]} == 'X' ]]; then
   case $mv in
      A) arr[0]=${arr[6]}
         arr[6]='X'
         ;;
      D) arr[0]=${arr[8]}
         arr[8]='X'
         ;;
   esac
elif [[ ${arr[8]} == 'X' ]]; then
   case $mv in
      A) arr[8]=${arr[5]}
         arr[5]='X'
         ;;
      C) arr[8]=${arr[0]}
         arr[0]='X'
         ;;
      D) arr[8]=${arr[7]}
         arr[7]='X'
         ;;
   esac
elif [[ ${arr[7]} == 'X' ]]; then
   case $mv in
      A) arr[7]=${arr[4]}
         arr[4]='X'
         ;;
      C) arr[7]=${arr[8]}
         arr[8]='X'
         ;;
   esac
elif [[ ${arr[6]} == 'X' ]]; then
   case $mv in
      A) arr[6]=${arr[3]}
         arr[3]='X'
         ;;
      B) arr[6]=${arr[0]}
         arr[0]='X'
         ;;
      D) arr[6]=${arr[5]}
         arr[5]='X'
         ;;
   esac
elif [[ ${arr[5]} == 'X' ]]; then
   case $mv in
      A) arr[5]=${arr[2]}
         arr[2]='X'
         ;;
      B) arr[5]=${arr[8]}
         arr[8]='X'
         ;;
      C) arr[5]=${arr[6]}
         arr[6]='X'
         ;;
      D) arr[5]=${arr[4]}
         arr[4]='X'
         ;;
   esac
elif [[ ${arr[4]} == 'X' ]]; then
   case $mv in
      A) arr[4]=${arr[1]}
         arr[1]='X'
         ;;
      B) arr[4]=${arr[7]}
         arr[7]='X'
         ;;
      C) arr[4]=${arr[5]}
         arr[5]='X'
         ;;
   esac
elif [[ ${arr[3]} == 'X' ]]; then 
   case $mv in
      B) arr[3]=${arr[6]}
         arr[6]='X'
         ;;
      D) arr[3]=${arr[2]}
         arr[2]='X'
         ;;
   esac
elif [[ ${arr[2]} == 'X' ]]; then
   case $mv in
      B) arr[2]=${arr[5]}
         arr[5]='X'
         ;;
      C) arr[2]=${arr[3]}
         arr[3]='X'
         ;;
      D) arr[2]=${arr[1]}
         arr[1]='X'
         ;;
   esac
elif [[ ${arr[1]} == 'X' ]]; then
   case $mv in
      B) arr[1]=${arr[4]}
         arr[4]='X'
         ;;
      C) arr[1]=${arr[2]}
         arr[2]='X'
         ;;
      'P'|'p') arr[1]=1
         arr[2]=2
         arr[3]=3
         arr[4]=4
         arr[5]=5
         arr[6]=6
         arr[7]=7
         arr[8]=8
         arr[0]='X'
         ;;
   esac
fi
}
clear
shuffle
startt=$(date +%s)
count=0
until [[ ${arr[*]} == ${arrp[*]} ]];do

   screen_output
	move
   arrac=$(echo "${arr[*]}"| cut -d ' ' -f2-9)
   clear
   count=$((count + 1 ))
done
screen_output
endt=$(date +%s)
runtime=$((endt-startt))
count=$((count / 3 ))
printf "\t\t\t\t\t\tWinner winner, chicken dinner\n"
printf "\t\t\t\t\t\tCas hrania: %ss\n" "$runtime"
printf "\t\t\t\t\t\tPocet pohybov: $count \n\n\n"
